"""
Driver for OpenWPM.  This script must be executed with the OpenWPM repository
in PYTHONPATH and using the python executable from OpenWPM's conda installation
(or something equivalent).  This script has only been tested on OpenWPM
v0.21.1.
"""

import argparse
from pathlib import Path
import random
import shutil
import sys
import os

# from Commands.get_javascript import GetJavaScript
from openwpm.command_sequence import CommandSequence
from openwpm.commands.browser_commands import GetCommand,SaveScreenshotCommand
from openwpm.config import BrowserParams, ManagerParams
from openwpm.storage.sql_provider import SQLiteStorageProvider
from openwpm.storage.leveldb import LevelDbProvider
from openwpm.task_manager import TaskManager

parser = argparse.ArgumentParser(
    prog='python3 crawl.py',
    description='''
    Crawl a list of URLs with OpenWPM.  The openwpm conda environment must be
    active and the OpenWPM root directory must be in PYTHONPATH.  So a possible
    invocation from the shell would be
      $ conda activate openwpm
      (openwpm) $ PYTHONPATH=<path to OpenWPM> python3 crawl.py ...
    assuming that conda is in your path.
    ''',
    formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('datadir', type=str, action='store',
    help='''
    Data directory path; OpenWPM outputs will be saved in this directory.  If
    the directory already exists, it will be deleted.
    ''')
parser.add_argument('urlsfile', type=str, action='store',
    help='''
    File with list of URLs to crawl.  One URL per line.  If urlsfile ends
    in .html, then urlsfile must be a local HTML file, and OpenWPM will 
    crawl it instead of reading URLs from it.
    ''')
parser.add_argument('--sample', type=int, action='store', default=-1,
    help='''
    Number of URLs to crawl.  This many URLs will be chosen at random from
    urlsfile (which must be a list of URLs, not a local HTML file).  If -1,
    then crawl all the URLs in urlsfile. Default=%(default)s.
    ''')
parser.add_argument('--instrument', type=str, action='store',
    default='js',
    help='''
    Comma separated list of instruments to select.  For each item s in the
    list, s_instrument will be set to True for the browser parameters.  Every
    other instrument will be set to False.  Default = %(default)s.
    ''')
parser.add_argument('--save-content', type=str, action='store',
    default=None,
    help='''
    Types of HTTP request content to save in content-data.ldb LevelDB database.
    Specified as a comma-separated string.
    Do not save content if None.  Only useful if --instrument specifies http.
    Default = %(default)s.
    ''')
parser.add_argument('--screenshots', action='store_true',
    help='''
    Save screenshot of each visited site.
    Default = do not save screenshot.
    ''')
parser.add_argument('--num-browsers', type=int, default=1,
    help='''
    Number of browsers to launch.  Default = %(default)s.
    ''')

args = parser.parse_args()

# Directory in which to store OpenWPM crawl data.
data_directory = Path(args.datadir).resolve()
if os.access(data_directory, os.R_OK):
    print(f'Deleting existing {data_directory.absolute()}.')
    shutil.rmtree(data_directory.absolute())

# URLs to crawl.
if args.urlsfile.endswith('.html'):
    sites = [args.urlsfile]
else:
    urls_file = Path(args.urlsfile).resolve()
    if not os.access(urls_file, os.R_OK):
        print(f"URLs file {urls_file} not available for reading; exiting.")
        sys.exit(1)
    with urls_file.open('r') as f:
        sites = filter(lambda s: s.strip() != '', f.read().split('\n'))
        if args.sample != -1:
            sites = random.sample(sites, args.sample)

# Possible instruments, and instruments to activate.
instruments = ['http', 'js', 'navigation', 'callstack', 'dns', 'cookie']
selected_instruments = args.instrument.split(',')

# The number of browsers to use.
NUM_BROWSERS = args.num_browsers
manager_params = ManagerParams(num_browsers=NUM_BROWSERS)

# Configure all browser parameters identically.
browser_params = [BrowserParams(display_mode="headless") for _ in range(NUM_BROWSERS)]
for browser_param in browser_params:

    # Set instrumentation according to --instrument.
    for i in instruments:
        browser_param.__dict__[f'{i}_instrument'] = False
    for i in selected_instruments:
        browser_param.__dict__[f'{i}_instrument'] = True

    # What JS Web API calls to watch
    browser_param.js_instrument_settings = ["collection_fingerprinting"]

    # Save content according to --save-content.
    if args.save_content:
        browser_param.save_content = args.save_content

# Update TaskManager configuration (use this for crawl-wide settings)
manager_params.data_directory = data_directory
manager_params.log_path = data_directory.joinpath("openwpm.log")

# Database providers
sqlite_db_provider = \
    SQLiteStorageProvider(data_directory.joinpath("crawl-data.sqlite"))
level_db_provider = (
    LevelDbProvider(data_directory.joinpath("content-data.ldb"))
    if args.save_content is not None
    else None
)

# memory_watchdog and process_watchdog are useful for large scale cloud crawls.
# Please refer to docs/Configuration.md#platform-configuration-options for more information
# manager_params.memory_watchdog = True
# manager_params.process_watchdog = True


# Do the crawl.
with TaskManager(
    manager_params,
    browser_params,
    sqlite_db_provider,
    level_db_provider
) as manager:
    # Visits the sites
    for index, site in enumerate(sites):

        def callback(success: bool, val: str = site) -> None:
            print(
                f"CommandSequence for {val} ran {'successfully' if success else 'unsuccessfully'}"
            )   

        # Parallelize sites over all number of browsers set above.
        command_sequence = CommandSequence(
            site,
            site_rank=index,
            callback=callback,
        )

        # Visit the page
        command_sequence.append_command(GetCommand(url=site, sleep=2), timeout=30)
        if args.screenshots:
            command_sequence.append_command(
                SaveScreenshotCommand("")
            )

        # Run commands across all browsers (simple parallelization)
        manager.execute_command_sequence(command_sequence)

