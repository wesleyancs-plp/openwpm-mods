"""
dump-lvldb.py:  Script to dump the contents of a LevelDB database.
Requires jsbeautifier and plyvel modules to be in PYTHONPATH.
"""

import argparse
import logging
import multiprocessing as mp
import os
import shutil
import sys

import jsbeautifier
import plyvel

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''
        Dump a LevelDB database to a directory.  For each key-value pair
        in the database, a file named key will be created, with contents value.
        '''
    )
    parser.add_argument('db', type=str,
        help='''
        LevelDB database directory.
        ''')
    parser.add_argument('dir', type=str,
        help='''
        Directory to dump database to.
        ''')
    parser.add_argument('--only', type=str, action='store', default=None,
        help='''
        Comma separated list of keys to extract.  None to extract all.
        Default = %(default)s.
        ''')
    parser.add_argument('--only-from', type=str, action='store',
        default=None,
        help='''
        Filename with keys to extract, one per line.  Overrides --only.
        ''')
    parser.add_argument('--suffix', type=str, action='store', default=None,
        help='''
        Suffix for each filename (None for no suffix).  Default = %(default)s.
        ''')
    parser.add_argument('--clobber', action='store_true',
        help='''
        Delete existing dump directory if it already exists.
        ''')
    parser.add_argument('--beautify', action='store_true',
        help='''
        Beautify each value with jsbeautifier.
        ''')


    args = parser.parse_args()

    try:
        db_handle = plyvel.DB(args.db)
    except Exception as e:
        print(f'Opening database {args.db} raised error: {e}')
        sys.exit(1)

    try:
        if args.clobber and os.access(args.dir, os.R_OK):
            shutil.rmtree(args.dir)
            os.mkdir(args.dir)
        elif not os.access(args.dir, os.R_OK):
            os.mkdir(args.dir)

    except Exception as e:
        print(f'Creating directory {args.dir} raised error: {e}')
        sys.exit(1)

    # extract = [] if args.only is None else args.only.split(',')
    if args.only_from:
        extract = []
        with open(args.only_from, 'r') as f:
            for line in f:
                line = line.strip()
                if line != "":
                    extract.append(line)
    elif args.only:
        extract = args.only.split(',')
    else:
        extract = []

    def writekv(k : str, v : bytes) -> None:
        """
        Write bytes.decode(v) to file args.dir/k.
        """
        try:
            v = bytes.decode(v)
            kfile = k if args.suffix is None else f'{k}.{args.suffix}'
            kfile = args.dir + '/' + kfile
            logging.info(f'Dumping data to {kfile}')
            with open(kfile, 'w') as kf:
                kf.write(jsbeautifier.beautify(v) if args.beautify else v)
        except UnicodeDecodeError:
            logging.warning(f'Unicode decoding error for hash {k}')

    def do_extract(k):
        v = db_handle.get(bytes(k, encoding='utf-8'))
        if v is None:
            logging.warning(f'Skpping non-existent key {k}')
        else:
            logging.info(f'Dumping key {k}')
            writekv(k, v)

    def do_extractkv(kv):
        writekv(bytes.decode(kv[0]), kv[1])

    if extract != []:
        with mp.Pool() as p:
            p.map(do_extract, extract)
    else:
        with mp.Pool() as p:
            p.map(do_extractkv, db_handle)

    db_handle.close()

