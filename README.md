# Scripts for using OpenWPM

This repository contains scripts for using OpenWPM.  Files:

* `crawl.py`:  driver for OpenWPM.  The 
  [OpenWPM](https://github.com/openwpm/OpenWPM) root directory must be in
  `PYTHONPATH` when executing, and the `python` executable should be from
  either the OpenWPM `conda` installation of something equivalent.  Thus,
  execution should be something like
  ```bash
  $ conda activate openwpm
  $ PYTHONPATH=<path to openwpm> python3 crawl.py ...
  ```
* `dump-lvldb.py`: script to dump LevelDB to files.  Use this to extract the
  LevelDB database that is created when `crawl.py` is used with the 
  `--save-content` argument.

